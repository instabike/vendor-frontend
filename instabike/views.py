from django.shortcuts import render,render_to_response, RequestContext

def page(request, page_name):
	page_name = page_name.strip('/')
	if page_name == 'page1':
		return render_to_response('page/page1.html',
			context_instance=RequestContext(request))

	if request.is_ajax() == False:
		return render_to_response('base.html',
			context_instance=RequestContext(request))

	return render_to_response('page/' + page_name + '.html',
		context_instance=RequestContext(request))