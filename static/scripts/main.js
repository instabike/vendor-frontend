var app = angular.module('instabike', [
'ngRoute'
]);

app.config(['$routeProvider', '$locationProvider', '$httpProvider', '$interpolateProvider', function ($routeProvider, $locationProvider, $httpProvider, $interpolateProvider) {
	// For removing conflict with django syntax
	$interpolateProvider.startSymbol('{$');
    $interpolateProvider.endSymbol('$}');
	// mark all requests as xhr
	$httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';
	// use html5 history if available
	if(window.history && window.history.pushState){
	    $locationProvider.html5Mode(true);
	}

	$routeProvider
	.when("/page1", {
		template: "", 
		controller: "SpecialController"})
	.when("/", {
		templateUrl: "/page2", 
		controller: "TempController"})
	.when("/page2", {
		templateUrl: "/page2", 
		controller: "TempController"})
	.when("/page3", {
		templateUrl: "/page3", 
		controller: "TempController"})
	.when("/page4", {
		templateUrl: "/page4", 
		controller: "TempController"})
	.otherwise("/404", {template: "error", controller: "ErrorController"});

}]);